
--VODIMO RACUNA PRILIKOM KREIRANJA TABELA, PRVO ENTITETI PA ONDA VEZE, CISTO DA IMAMO NAPOMENU :D P

--select Katalog.Naziv, SUM(ISNULL(KolicinaPro,0))  as KolicinaPro, SUM(ISNULL(Proizvod.CenaPorcije,0) + ISNULL(Proizvod.CenaDekoracije,0))as cenaHran,  SUM(Proizvod.CenaDekoracije) as dek from katalog inner join K_Sadrzi on Katalog.IDKatalog = K_Sadrzi.IDKatalog inner join Proizvod on K_Sadrzi.IDProizvoda = Proizvod.IDProizvoda
--where Katalog.IDKatalog = 1002 
--GROUP BY Katalog.Naziv; 
-- VEZA D_SADRZI

CREATE TABLE [dbo].[D_Sadrzi]
(
 
 [IDDogadjaja]   int NOT NULL ,
 [IDKatalog]     int NOT NULL ,


 CONSTRAINT [PK_D_Sadrzi] PRIMARY KEY CLUSTERED ([IDDogadjaja] ASC, [IDKatalog] ASC) ,
 CONSTRAINT [FK_107] FOREIGN KEY ([IDDogadjaja])  REFERENCES [dbo].[Dogadjaj]([IDDogadjaja]),
 CONSTRAINT [FK_111] FOREIGN KEY ([IDKatalog])  REFERENCES [dbo].[Katalog]([IDKatalog])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_107] ON [dbo].[D_Sadrzi] 
 (
  [IDDogadjaja] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_111] ON [dbo].[D_Sadrzi] 
 (
  [IDKatalog] ASC
 )

GO


-------------------------------------------------------------------------------------------

--DOGADJAJI

CREATE TABLE [dbo].[Dogadjaj]
(
 [IDDogadjaja]    int NOT NULL identity(1,1) ,
 [VrstaDogadjaja] nvarchar(30) NOT NULL ,


 CONSTRAINT [PK_Dogadjaj] PRIMARY KEY CLUSTERED ([IDDogadjaja] ASC)  with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



-------------------------------------------------------------------

-- VEZA K_SADRZI

CREATE TABLE [dbo].[K_Sadrzi]
(
 [KolicinaPro] int NOT NULL ,
 [IDKatalog]   int NOT NULL ,
 [IDProizvoda] int NOT NULL ,


 CONSTRAINT [PK_K_Sadrzi] PRIMARY KEY CLUSTERED ([IDKatalog] ASC, [IDProizvoda] ASC),
 CONSTRAINT [FK_114] FOREIGN KEY ([IDKatalog])  REFERENCES [dbo].[Katalog]([IDKatalog]),
 CONSTRAINT [FK_117] FOREIGN KEY ([IDProizvoda])  REFERENCES [dbo].[Proizvod]([IDProizvoda])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_114] ON [dbo].[K_Sadrzi] 
 (
  [IDKatalog] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_117] ON [dbo].[K_Sadrzi] 
 (
  [IDProizvoda] ASC
 )

GO


----------------------------------------------

-- KATALOG


CREATE TABLE [dbo].[Katalog]
(
 [IDKatalog] int NOT NULL identity(1,1) ,
 [Naziv]     nvarchar(50) NOT NULL ,


 CONSTRAINT [PK_Katalog] PRIMARY KEY CLUSTERED ([IDKatalog] ASC)  with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


------------------------

--KORISNIK


CREATE TABLE [dbo].[Korisnik]
(
 [IDKorisnika]  int NOT NULL identity(1,1) ,
 [Ime]          nvarchar(60) NOT NULL ,
 [Prezime]      nvarchar(50) NULL ,
 [Email]        nvarchar(50) NOT NULL ,
 [Username]     nvarchar(50) NOT NULL ,
 [Sifra]        nvarchar(50) NOT NULL ,
 [BrojTelefona] nvarchar(50) NULL ,
 [Tip]          nvarchar(40) NOT NULL ,


 CONSTRAINT [PK_Korisnik] PRIMARY KEY CLUSTERED ([IDKorisnika] ASC) with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--------------------------------------
--VEZA N_SADRZI

CREATE TABLE [dbo].[N_Sadrzi]
(
 [IDNarudzbine] int NOT NULL ,
 [IDKatalog]    int NOT NULL ,
 [Kolicina]     int NOT NULL ,


 CONSTRAINT [PK_N_Sadrzi] PRIMARY KEY CLUSTERED ([IDNarudzbine] ASC, [IDKatalog] ASC),
 CONSTRAINT [FK_103] FOREIGN KEY ([IDKatalog])  REFERENCES [dbo].[Katalog]([IDKatalog]),
 CONSTRAINT [FK_96] FOREIGN KEY ([IDNarudzbine])  REFERENCES [dbo].[Narudzbina]([IDNarudzbine])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_103] ON [dbo].[N_Sadrzi] 
 (
  [IDKatalog] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_96] ON [dbo].[N_Sadrzi] 
 (
  [IDNarudzbine] ASC
 )

GO


-------------------------------------------------

--NARUDZBINA

CREATE TABLE [dbo].[Narudzbina]
(
 [IDNarudzbine]      int NOT NULL identity(1,1) ,
 [Datum_Narucivanja] date NOT NULL ,
 [Datum_Isporuke]    date NOT NULL ,
 [Vreme_Isporuke]    time NOT NULL,
 [Adresa_Isporuke]   NVARCHAR(50) NOT NULL,
 [UkupnaCena]        decimal(6,0) NOT NULL ,
 [IDRadnika]         int NOT NULL ,
 [IDKorisnika]       int NOT NULL ,


 CONSTRAINT [PK_Narudzbina] PRIMARY KEY CLUSTERED ([IDNarudzbine] ASC)  with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [FK_127] FOREIGN KEY ([IDKorisnika])  REFERENCES [dbo].[Korisnik]([IDKorisnika]),
 CONSTRAINT [FK_87] FOREIGN KEY ([IDRadnika])  REFERENCES [dbo].[Radnik]([IDRadnika])
) 
GO


CREATE NONCLUSTERED INDEX [fkIdx_127] ON [dbo].[Narudzbina] 
 (
  [IDKorisnika] ASC
 )

GO

CREATE NONCLUSTERED INDEX [fkIdx_87] ON [dbo].[Narudzbina] 
 (
  [IDRadnika] ASC
 )

GO


----------------------------------------

--PROIZVOD
CREATE TABLE [dbo].[Proizvod]
(
 [IDProizvoda]     int NOT NULL identity(1,1),
 [Naziv]           nvarchar(50) NOT NULL ,
 [Opis]            nvarchar(100) NOT NULL ,
 [Tip]             int NOT NULL ,
 [VrstaDekoracije] nvarchar(50) NULL ,
 [Boja]            nvarchar(20) NULL ,
 [CenaDekoracije]  decimal(5,0) NULL ,
 [VrstaObroka]     nvarchar(50) NULL ,
 [Gramaza]         decimal(18,0) NULL ,
 [CenaPorcije]     decimal(5,0) NULL ,
 [PutanjaDoSlike]  NVARCHAR (100) NULL

 CONSTRAINT [PK_Proizvod] PRIMARY KEY CLUSTERED ([IDProizvoda] ASC)  with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO



---------------------

--RADNIK

CREATE TABLE [dbo].[Radnik]
(
 [IDRadnika] int NOT NULL IDENTITY(1,1),
 [Ime]       nvarchar(50) NOT NULL ,
 [Prezime]   nvarchar(50) NOT NULL ,
 [Telefon]   NVARCHAR (15) null,
 [Slobodan]  BIT  not null, 

 CONSTRAINT [PK_Radnik] PRIMARY KEY CLUSTERED ([IDRadnika] ASC)  with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
);
GO


-----------------------

-- Za tabelu Dekoracija koja je izvedena iz Proizvod i ovde je samo
--stavljena da je ima, inace tabela Dekoracija i Hrana
--su koristeci Alternativu C iz sistema baza smestena u 
-- tab Proizvod gde tip odredjuje 0-hrana, 1-dekoracija ili obrnuto, kako se dog 

CREATE TABLE [dbo].[Dekoracija]
(
 [IDDekoracije]    int NOT NULL,
 [VrstaDekoracije] nvarchar(50) NOT NULL ,
 [Boja]            nvarchar(30) NOT NULL ,
 [CenaDekoracije]  decimal(18,0) NOT NULL ,


 CONSTRAINT [PK_Dekoracija] PRIMARY KEY CLUSTERED ([IDDekoracije] ASC)
);
GO

-- HRANA

CREATE TABLE [dbo].[Hrana]
(
 [VrstaObroka] nvarchar(50) NOT NULL ,
 [IDHrane]     int NOT NULL ,
 [Gramaza]     decimal(5,0) NOT NULL ,
 [CenaPorcije] decimal(10,0) NOT NULL ,


 CONSTRAINT [PK_Hrana] PRIMARY KEY CLUSTERED ([IDHrane] ASC)
);
GO



--PODACI MORACEMO RUCNO DA UBACUJEMO ZA POCETAK DOK NE PODIGNEMO APK 


-------------------------------------------------------------------
--Tabela RADNIK
-------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Radnik] ON
GO

INSERT INTO dbo.Radnik ([IDRadnika], [Ime], [Prezime], [Telefon], [Slobodan])
VALUES (1, 'Petar', 'Jovanović', '0604528695', 1),
	(2, 'Jovan', 'Marković', '0612856945', 1),
	(3, 'Marko', 'Pešić', '0623645878', 1),
	(4, 'Nikola', 'Simić', '0636185994', 1),
	(5, 'Stefan', 'Pavlović', '0654816512', 1),
	(6, 'Tamara', 'Tošić', '0602879512', 1),
	(7, 'Andjela', 'Mitić', '0618213754', 1),
	(8, 'Milica', 'Nedeljković', '0694821653', 1),
	(9, 'Sanja', 'Petrović', '0636128457', 1),
	(10, 'Maša', 'Milovanović', '0624184563', 1)
GO

SET IDENTITY_INSERT [dbo].[Radnik] OFF
GO

-------------------------------------------------------------------
--Tabela KORISNIK
-------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Korisnik] ON
GO

INSERT INTO dbo.Korisnik ([IDKorisnika], [Ime], [Prezime], [Email], [Username], [Sifra], [BrojTelefona], [Tip])
VALUES (1, 'Vuk', 'Mitić', 'vukmitic@gmail.com', 'vuk123', 'vuk123', '0605421878', 'korisnik'),
	(2, 'Dimitrije', 'Savić', 'dimitrijesavic@gmail.com', 'dimacar', 'dimacar', '0605856187', 'korisnik'),
	(3, 'Lazar', 'Stojanović', 'lazarstojanovic@gmail.com', 'lakilaza', 'lakilaza', '0605474965', 'korisnik'),
	(4, 'Ana', 'Stanojević', 'anastanojevic@gmail.com', 'anci99', 'anci99', '0656428551', 'korisnik'),
	(5, 'Emilija', 'Denčić', 'emilijadencic@gmail.com', 'emainole', 'emainole', '0645788994', 'korisnik'),
	(6, 'Sara', 'Nikolić', 'saranikolic@gmail.com', 'sarasaki', 'sarasaki', '0614258692', 'korisnik'),
	(7, 'Nemanja', 'Aleksić', 'nemanjaaleksic@gmail.com', 'nemanjamenadzer', 'nemanjamenadzer', '0608249556', 'menadžer'),
	(8, 'Milena', 'Filipović', 'milenafilipovic@gmail.com', 'milenamenadzer', 'milenamenadzer', '0645122784', 'menadžer')
GO	
	
SET IDENTITY_INSERT [dbo].[Korisnik] OFF
GO
	
-------------------------------------------------------------------
--Tabela PROIZVOD
-------------------------------------------------------------------
	
SET IDENTITY_INSERT [dbo].[Proizvod] ON
GO
	
INSERT INTO dbo.Proizvod ([IDProizvoda], [Naziv], [Opis], [Tip], [VrstaDekoracije], [Boja], [CenaDekoracije], [VrstaObroka], [Gramaza], [CenaPorcije], [PutanjaDoSlike])	
VALUES (1, 'Ruska salata', 'mešano povrće sa majonezom', 0, null, null, null, ' hladno predjelo', 100, 150, "/images/Slike za aplikaciju/ruska_salata.jpg"),
	(2, 'Daska sa sirevima', 'kačkavalj, beli sir, kajmak, sir iz ulja', 0, null, null, null, 'hladno predjelo', 100, 200, "/images/Slike za aplikaciju/daska_sa_sirevima.jpg"),
	(3, 'Daska sa pršutama', 'razne vrste pršute', 0, null, null, null, 'hladno predjelo', 100, 250, "/images/Slike za aplikaciju/daska_sa_prsutama.jpg"),
	(4, 'Proja', 'proja sa spanaćem ili sirom', 0, null, null, null, 'toplo predjelo', 80, 100, "/images/Slike za aplikaciju/proja.jpg"),
	(5, 'Srpska gibanica', 'pita sa sukanim korama punjena sirom', 0, null, null, null, 'toplo predjelo', 100, 120, "/images/Slike za aplikaciju/srpska_gibanica.jpg"),
	(6, 'Pohovani kačkavalj', 'parče kačkavalja pohovano u jajima', 0, null, null, null, 'toplo predjelo', 100, 150, "/images/Slike za aplikaciju/pohovani_kackavalj.jpg"),
	(7, 'Pohovane tikvice', 'kolutovi tikvice pohovani u jajima', 0, null, null, null, 'toplo predjelo', 100, 150, "/images/Slike za aplikaciju/pohovane_tikvice.jpg"),
	(8, 'Šopska salata', 'krastavac, paradajz, crni luk, mrvljeni sir', 0, null, null, null, 'salata', 150, 150, "/images/Slike za aplikaciju/sopska_salata.jpg"),
	(9, 'Bašta salata', 'sezonsko povrće', 0, null, null, null, 'salata', 150, 120, "/images/Slike za aplikaciju/basta-salata.jpg"),
	(10, 'Tarator salata', 'krastavac, kiselo mleko, beli luk', 0, null, null, null, 'salata', 150, 150, "/images/Slike za aplikaciju/tarator_salata.jpg"),
	(11, 'Moravska salata', 'pečene paprike, paradajz, beli luk', 0, null, null, null, 'salata', 150, 150, "/images/Slike za aplikaciju/moravska_salata.jpg"),
	(12, 'Lepinja', 'pečena lepinja', 0, null, null, null, 'pecivo', 50, 30, "/images/Slike za aplikaciju/lepinja.jpg"),
	(13, 'Bavarske kiflice', 'bavarske kiflice', 0, null, null, null, 'pecivo', 80, 50, "/images/Slike za aplikaciju/bavarske_kiflice.jpg"),
	(14, 'Štapići sa susamom', 'štapići sa susamom', 0, null, null, null, 'pecivo', 50, 70, "/images/Slike za aplikaciju/stapici_sa_susamom.jpg"),
	(15, 'Koktel roštilj', 'pljeskavice, rolovano belo meso u slanini, leskovački uštipci, kobasice', 0, null, null, null, 'glavno jelo', 300, 700, "/images/Slike za aplikaciju/koktel_rostilj.jpg"),
	(16, 'Praseće pečenje', 'prase pečeno na ražnju', 0, null, null, null, 'glavno jelo', 300, 1000, "/images/Slike za aplikaciju/prasece_pecenje.jpg"),
	(17, 'Jagnjeće pečenje', 'jagnje pečeno na ražnju', 0, null, null, null, 'hladno predjelo', 300, 1000, "/images/Slike za aplikaciju/jagnjece-pecenje.jpg"),
	(18, 'File lososa', 'file lososa sa svežom mirodjijom u ren sosu', 0, null, null, null, 'glavno jelo', 250, 1100, "/images/Slike za aplikaciju/file-lososa.jpg"),
	(19, 'Somovina', 'somovina sa bademom, lešnikom i korn flexom', 0, null, null, null, 'glavno jelo', 250, 1000, "/images/Slike za aplikaciju/somovina.jpg"),
	(20, 'Pačetina', 'pačetina u kompotu od višanja sa pireom od kestena', 0, null, null, null, 'glavno jelo', 300, 1200, "/images/Slike za aplikaciju/pacetina.jpg"),
	(21, 'Pomfrit', 'prženi štapići krompira', 0, null, null, null, 'prilog', 100, 100, "/images/Slike za aplikaciju/pomfrit.jpg"),
	(22, 'Pečeni krompir', 'pečeni krompir začinjen suvim začinom', 0, null, null, null, 'prilog', 100, 100, "/images/Slike za aplikaciju/peceni-krompir.jpg"),
	(23, 'Grilovano povrće', 'tikvica, plavi paradajz, šargarepa, paprika', 0, null, null, null, 'prilog', 80, 50, "/images/Slike za aplikaciju/grilovano-povrce.jpg"),
	(24, 'Čokoladna torta', 'parče čokoladne torte', 0, null, null, null, 'dezert', 150, 230, "/images/Slike za aplikaciju/cokoladna-torta.jpg"),
	(25, 'Voćna torta', 'parče voćne torte', 0, null, null, null, 'dezert', 150, 200, "/images/Slike za aplikaciju/vocna-torta.jpg"),
	(26, 'Voćne bombice', 'sitni kolači sa voćnim punjenjem', 0, null, null, null, 'dezert', 100, 120, "/images/Slike za aplikaciju/vocne-bombice.jpg"),
	(27, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'plava', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-plava.jpg"),
	(28, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'žuta', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-zuta.jpg"),
	(29, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'zelena', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-zelena.jpg"),
	(30, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'crvena', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-crvena.jpg"),
	(31, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'ljubičasta', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-ljubicasta.jpg"),
	(32, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'roze', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-roze.jpg"),
	(33, 'Baloni brojevi', 'baloni u obliku odgovarajućeg broja', 1, 'balon', 'bela', 200, null, null, null, "/images/Slike za aplikaciju/baloni-brojevi-bela.jpg"),
	(34, 'Baloni', 'obični baloni', 1, 'balon', 'po dogovoru', 200, null, null, null, "/images/Slike za aplikaciju/baloni-obicni.jpg"),
	(35, 'Pano', 'pozadina za slikanje', 1, 'pano', 'plava', 500, null, null, null, "/images/Slike za aplikaciju/pano-plava.jpg"),
	(36, 'Pano', 'pozadina za slikanje', 1, 'pano', 'žuta', 500, null, null, null, "/images/Slike za aplikaciju/pano-zuta.jpg"),
	(37, 'Pano', 'pozadina za slikanje', 1, 'pano', 'zelena', 500, null, null, null, "/images/Slike za aplikaciju/pano-zelena.jpg"),
	(38, 'Pano', 'pozadina za slikanje', 1, 'pano', 'crvena', 500, null, null, null, "/images/Slike za aplikaciju/pano-crvena.jpg"),
	(39, 'Pano', 'pozadina za slikanje', 1, 'pano', 'ljubičasta', 500, null, null, null, "/images/Slike za aplikaciju/pano-ljubicasta.jpg"),
	(40, 'Pano', 'pozadina za slikanje', 1, 'pano', 'roze', 500, null, null, null, "/images/Slike za aplikaciju/pano-roze.jpg"),
	(41, 'Pano', 'pozadina za slikanje', 1, 'pano', 'bela', 500, null, null, null, "/images/Slike za aplikaciju/pano-bela.jpg"),
	(42, 'Ukrasna slova', 'slova od stiropora', 1, 'slova', 'po dogovoru', 200, null, null, null, "/images/Slike za aplikaciju/ukrasna-slova.jpg"),
	(43, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'plava', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-plava.jpg"),
	(44, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'žuta', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-zuta.jpg"),
	(45, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'zelena', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-zelena.jpg"),
	(46, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'crvena', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-crvena.jpg"),
	(47, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'ljubičasta', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-ljubicasta.jpg"),
	(48, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'roze', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-roze.jpg"),
	(49, 'Slatki sto', 'sto sa dekoracijom, slatkišima i pićem', 1, 'slatki sto', 'bela', 2500, null, null, null, "/images/Slike za aplikaciju/slatki-sto-bela.jpg")
GO	
	
SET IDENTITY_INSERT [dbo].[Proizvod] OFF
GO	

-------------------------------------------------------------------
--Tabela KATALOG
-------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Katalog] ON
GO
	
INSERT INTO dbo.Katalog ([IDKatalog], [Naziv])	
VALUES (1, 'Švedski sto za rodjendan'),
	(2, 'Švedski sto za svadbu'),
	(3, 'Švedski sto za slavu'),
	(4, 'Švedski sto za zaposlene'),
	(11, 'Vip meni'),
	(12, 'Platinum meni'),
	(5, 'Plava laguna'),
	(6, 'Žuti zalazak'),
	(7, 'Zelena oaza'),
	(8, 'Vatrovita crvena'),
	(9, 'Rojalna ljubičasta'),
	(10, 'Bejbi pink'),
	(13, 'Elegantno bela')
GO	
	
SET IDENTITY_INSERT [dbo].[Katalog] OFF
GO	


--------------------------------------------------------------------
--Tabela K_SADRZI
--------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[K_Sadrzi] ON
GO

INSERT INTO dbo.K_Sadrzi ([IDKatalog], [IDProizvoda], [KolicinaPro])
VALUES (1, 1, 1),
	(1, 4, 1),
	(1, 8, 1),
	(1, 9, 1),
	(1, 13, 1),
	(1, 14, 1),
	(1, 15, 1),
	(1, 18, 1),
	(1, 21, 1),
	(1, 22, 1),
	(1, 23, 1),
	(1, 24, 1),
	(1, 25, 1),
	(1, 26, 1),
	(2, 1, 1),
	(2, 2, 1),
	(2, 3, 1),
	(2, 5, 1),
	(2, 6, 1),
	(2, 8, 1),
	(2, 10, 1),
	(2, 11, 1),
	(2, 12, 1),
	(2, 15, 1),
	(2, 16, 1),
	(2, 17, 1),
	(2, 19, 1),
	(2, 21, 1),
	(2, 22, 1),
	(2, 23, 1),
	(2, 24, 1),
	(3, 1, 1),
	(3, 2, 1),
	(3, 5, 1),
	(3, 7, 1),
	(3, 9, 1),
	(3, 11, 1),
	(3, 12, 1),
	(3, 15, 1),
	(3, 16, 1),
	(3, 19, 1),
	(3, 21, 1),
	(3, 22, 1),
	(3, 26, 1),
	(4, 2, 1),
	(4, 3, 1),
	(4, 6, 1),
	(4, 7, 1),
	(4, 8, 1),
	(4, 13, 1),
	(4, 14, 1),
	(4, 15, 1),
	(4, 18, 1),
	(4, 21, 1),
	(4, 23, 1),
	(4, 25, 1),
	(11, 2, 1),
	(11, 3, 1),
	(11, 6, 1),
	(11, 7, 1),
	(11, 10, 1),
	(11, 11, 1),
	(11, 12, 1),
	(11, 13, 1),
	(11, 14, 1),
	(11, 15, 1),
	(11, 16, 1),
	(11, 18, 1),
	(11, 19, 1),
	(11, 21, 1),
	(11, 22, 1),
	(11, 23, 1),
	(11, 25, 1),
	(11, 26, 1),
	(12, 2, 1),
	(12, 3, 1),
	(12, 5, 1),
	(12, 6, 1),
	(12, 7, 1),
	(12, 8, 1),
	(12, 9, 1),
	(12, 10, 1),
	(12, 12, 1),
	(12, 13, 1),
	(12, 14, 1),
	(12, 16, 1),
	(12, 17, 1),
	(12, 18, 1),
	(12, 19, 1),
	(12, 20, 1),
	(12, 21, 1),
	(12, 22, 1),
	(12, 23, 1),
	(12, 24, 1),
	(12, 25, 1),
	(12, 26, 1),
	(5, 27, 1),
	(5, 34, 20),
	(5, 35, 1),
	(5, 42, 1),
	(5, 43, 1),
	(6, 28, 1),
	(6, 34, 20),
	(6, 36, 1),
	(6, 42, 1),
	(6, 44, 1),
	(7, 29, 1),
	(7, 34, 20),
	(7, 37, 1),
	(7, 42, 1),
	(7, 45, 1),
	(8, 30, 1),
	(8, 34, 20),
	(8, 38, 1),
	(8, 42, 1),
	(8, 46, 1),
	(9, 31, 1),
	(9, 34, 20),
	(9, 39, 1),
	(9, 42, 1),
	(9, 47, 1),
	(10, 32, 1),
	(10, 34, 20),
	(10, 40, 1),
	(10, 42, 1),
	(10, 48, 1),
	(11, 33, 1),
	(11, 34, 20),
	(11, 41, 1),
	(11, 42, 1),
	(11, 49, 1)
GO

SET IDENTITY_INSERT [dbo].[K_Sadrzi] OFF
GO	
--------------------------------------------------------------------
--Tabela DOGADJAJI
--------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Dogadjaj] ON
GO

INSERT INTO dbo.Dogadjaj ([IDDogadjaja], [VrstaDogadjaja])
VALUES (1, 'rodjendan'),
	(2, 'svadba'),
	(3, 'slava'),
	(4, 'koktel'),
	(5, 'krstenje'),
	(6, 'matura'),
	(7, 'vrtic')
GO

SET IDENTITY_INSERT [dbo].[Dogadjaj] OFF
GO	

--------------------------------------------------------------------
--Tabela D_SADRZI
--------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[D_Sadrzi] ON
GO	

INSERT INTO dbo.D_Sadrzi([IDDogadjaja], [IDKatalog])
VALUES (1, 1),
	(1, 11),
	(1, 12),
	(2, 2),
	(2, 11),
	(2, 12),
	(3, 3),
	(3, 11),
	(3, 12),
	(4, 4),
	(5, 1),
	(5, 11),
	(5, 12),
	(6, 2),
	(6, 11),
	(6, 12),
	(7, 1),
	(7, 12)
GO

SET IDENTITY_INSERT [dbo].[D_Sadrzi] OFF
GO	

--------------------------------------------------------------------
--Tabela NARUDZBINA
--------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[Narudzbina] ON
GO

INSERT INTO dbo.Narudzbina([IDNarudzbine], [Datum_Narucivanja], [Datum_Isporuke], [Vreme_Isporuke], [Adresa_Isporuke], [UkupnaCena], [IDRadnika], [IDKorisnika])
VALUES (1,'2020-05-08','2020-05-20','14:00:00','7. juli 5',5000,1,1),
	(2,'2020-06-02','2020-06-18','18:00:00','Dušanova 54',7000,2,3),
	(3,'2020-05-14','2020-06-01','13:00:00','Aleksandra Medvedeva 7',10000,4,5)
GO

SET IDENTITY_INSERT [dbo].[Narudzbina] OFF
GO

--------------------------------------------------------------------
--Tabela N_SADRZI
--------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[N_Sadrzi] ON
GO

INSERT INTO dbo.N_Sadrzi([IDNarudzbine], [IDKatalog], [Kolicina])
VALUES (1,1,20),
	(1,5,1),
	(2,11,40),
	(2,13,1),
	(3,12,50),
	(3,9,1)
GO

SET IDENTITY_INSERT [dbo].[N_Sadrzi] OFF
GO


//tabela za pocetne podatkee//// 
GO
CREATE TABLE [dbo].[PocetniPodaci] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [ONama]               VARCHAR (3000) NULL,
    [KorisnickaUsluga]    VARCHAR (3000) NULL,
    [UsloviKoriscenja]    VARCHAR (3000) NULL,
    [PolitikaPrivatnost]  VARCHAR (3000) NULL,
    [InformacijeODostavi] VARCHAR (3000) NULL,
	CONSTRAINT [PK_Pocetni] PRIMARY KEY CLUSTERED ([Id] ASC)  with (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];

///////////////////////////////////////////////////


//podaci////
  INSERT into PocetniPodaci ([ONama],[KorisnickaUsluga],[UsloviKoriscenja],[PolitikaPrivatnost],[InformacijeODostavi])
  values (' Naša misija je pravovremena isporuka kvalitetne i bezbedne ketering usluge, kako kompanijama, tako i svim drugim licima 24 časa, sedam dana u nedelji.
                Pružamo kompletnu uslugu, bez obzira na broj zvanica i vrste svečanosti. Raspolažemo kapacitetima kojima možemo da se uhvatimo u koštac sa velikim privatnim
                i poslovnim proslavama. Posedujemo HACCP standard kvaliteta.
                Zahvaljujući moderno opremljenoj kuhinji sa pratećim sadržajima, možete računati na prvoklasan ketering i savršeno oraganizovanu zabavu. Firma Dionis
                ništa ne prepušta slučaju, te u skladu sa veličinom događaja i zahtevima klijenata angažuje svo potrebno osoblje. Sigurni smo da ćete biti zadovoljni našom uslugom, zato računamo na Vašu preporuku.
         ','  Bez obzira na vrstu događaja izlazimo u susret Vašim zahtevima kako po pitanju keteringa, tako i po pitanju
                organizacije. Bilo da želite da organizujete veliku korporativnu proslavu, porodičnu proslavu ili da organizujete
                dnevnu ishranu svojih zaposlenih, pružamo Vam kompletnu uslugu keteringa i organizacije.
                
                Ponuda ketering usluga:
                -Privatne proslave
                -Ishrana zaposlenih
                -Kantine i restorani
                -Korporativni ketering i ishrana zaposlenih
                -Dečiji rođendani
                -Vrtići i škole
                
                Usluga organizacije događaja:
                -Iznajmljivanje opreme
                -Dekoracija prostora
                
                Ukoliko Vam je potrebna usluga koje nema u ponudi kontaktirajte nas i skrojićemo ponudu/meni prema Vašim potrebama.
                ','   Korišćenjem ove web aplikacije, prihvatate ove Uslove korišćenja kao i pravno obavezujući odnos koji se konstituiše između vas kao Korisnika i kompanije Dionis,
                koja je vlasnik ove web aplikacije. Prilikom zasnivanja pretplatničkog odnosa, prihvatanje ovih Uslova korišćenja smatra se zaključenjem ugovora na daljinu u smislu Zakona o zaštiti potrošača.
         ',' Informacije koje nam date čuvaju se na našem serveru, a mogu im pristupiti naši saradnici, državni organi, naši pravni sledbenici i lica koja angažujemo da obrađuju
                podatke u naše ime u svrhe navedene u ovoj politici ili druge svrhe za koje ste Vi dali odobrenje. Takođe, možemo preneti informacije o upotrebi našeg internet sajta trećim
                licima ali to neće uključivati informacije na osnovu kojih Vi možete biti identifikovani. Osim ako to zakon ne nalaže, nećemo ni na koji način učiniti dostupnim niti distribuirati
                informacije o Vama koje nam pružite, bez vašeg prethodnog odobrenja.',' Osim standardne dostave keteringa, Dionis vam omogućava da po narudžbini dobijete isporuku robe široke potrošnje za kancelarije i firme na teritoriji grada Niša. Narudžbina može da bude
                sastavljena od hrane, voća, pića pa čak i sredstava za higijenu.');

//

SET IDENTITY_INSERT [dbo].[PocetniPodaci] ON
INSERT INTO [dbo].[PocetniPodaci] ([Id], [ONama], [KorisnickaUsluga], [UsloviKoriscenja], [PolitikaPrivatnost], [InformacijeODostavi]) VALUES (2,
'Naša misija je pravovremena isporuka kvalitetne i bezbedne ketering usluge, kako kompanijama, tako i svim drugim licima 24 casa, sedam dana u nedelji.  Pružamo kompletnu uslugu, bez obzira na broj zvanica i vrste svecanosti. Raspolažemo kapacitetima kojima možemo da se uhvatimo u koštac sa velikim privatnim i poslovnim proslavama. Posedujemo HACCP standard kvaliteta. Zahvaljujuci moderno opremljenoj kuhinji sa pratecim sadržajima, možete racunati na prvoklasan ketering i savršeno oraganizovanu zabavu. Firma Dionis ništa ne prepušta slucaju, te u skladu sa velicinom dogadaja i zahtevima klijenata angažuje svo potrebno osoblje. Sigurni smo da cete biti zadovoljni našom uslugom, zato racunamo na Vašu preporuku. ', 'Bez obzira na vrstu dogadaja izlazimo u susret Vašim zahtevima kako po pitanju keteringa, tako i po pitanju organizacije. Bilo da želite da organizujete veliku korporativnu proslavu, porodicnu proslavu ili da organizujete dnevnu ishranu svojih zaposlenih, pružamo Vam kompletnu uslugu keteringa i organizacije. Ponuda ketering usluga:    -Privatne proslave       -Ishrana zaposlenih                -Kantine i restorani                -Korporativni ketering i ishrana zaposlenih                -Deciji rodendani                 -Vrtici i škole                    Usluga organizacije dogadaja:                -Iznajmljivanje opreme              -Dekoracija prostora                             Ukoliko Vam je potrebna usluga koje nema u ponudi kontaktirajte nas i skrojicemo ponudu/meni prema Vašim potrebama.     ','   Korišcenjem ove web aplikacije, prihvatate ove Uslove korišcenja kao i pravno obavezujuci odnos koji se konstituiše izmedu vas kao Korisnika i kompanije Dionis,
                koja je vlasnik ove web aplikacije. Prilikom zasnivanja pretplatnickog odnosa, prihvatanje ovih Uslova korišcenja smatra se zakljucenjem ugovora na daljinu u smislu Zakona o zaštiti potrošaca.
         ',' Informacije koje nam date cuvaju se na našem serveru, a mogu im pristupiti naši saradnici, državni organi, naši pravni sledbenici i lica koja angažujemo da obraduju
                podatke u naše ime u svrhe navedene u ovoj politici ili druge svrhe za koje ste Vi dali odobrenje. Takode, možemo preneti informacije o upotrebi našeg internet sajta trecim
                licima ali to nece ukljucivati informacije na osnovu kojih Vi možete biti identifikovani. Osim ako to zakon ne nalaže, necemo ni na koji nacin uciniti dostupnim niti distribuirati
                informacije o Vama koje nam pružite, bez vašeg prethodnog odobrenja.',' Osim standardne dostave keteringa, Dionis vam omogucava da po narudžbini dobijete isporuku robe široke potrošnje za kancelarije i firme na teritoriji grada Niša. Narudžbina može da bude
                sastavljena od hrane, voca, pica pa cak i sredstava za higijenu.')
SET IDENTITY_INSERT [dbo].[PocetniPodaci] OFF

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
dodao kolonu u katalog i u narudzbinu 
alter table dbo.Narudzbina add StatusNarudzbine int null; za narudzbinu
alter table dbo.Katalog add Kreirao int null; 0 menadzer i na osnovu to ce da prikaze menije koje su menadzer/i kreirali @ 1 korisnik
a status narudzbine je 0 kreirana,1 odbijena, 2 isporucena

//za kolicinu u katalogu
select Katalog.IDKatalog, SUM(K_Sadrzi.KolicinaPro * Proizvod.CenaPorcije) as KolicinaaMori
from Katalog inner join K_Sadrzi on Katalog.IDKatalog = K_Sadrzi.IDKatalog
inner join Proizvod on K_Sadrzi.IDProizvoda=Proizvod.IDProizvoda 
where Proizvod.Tip=0 
group by Katalog.IDKatalog
Order by Katalog.IDKatalog asc;



